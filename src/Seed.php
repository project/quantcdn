<?php

namespace Drupal\quant;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\quant\Event\QuantEvent;
use Drupal\quant\Event\QuantRedirectEvent;
use Drupal\taxonomy\Entity\Term;
use Drupal\views\Views;
use GuzzleHttp\Exception\ConnectException;

/**
 * Seed Manager.
 *
 * The workhorse of Quant, responsible for orchestrating Drupal events and
 * emitting Quant module events so that content can be pushed to the edge.
 *
 * @todo define as a service and use dependency injection.
 */
class Seed {

  /**
   * Batch finish callback for the seed.
   */
  public static function finishedSeedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One item processed.', '@count items processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Find Lunr assets.
   *
   * This includes static output from the Lunr module.
   */
  public static function findLunrAssets() {
    $scheme = \Drupal::config('system.file')->get('default_scheme');
    $filesPath = \Drupal::service('file_system')->realpath($scheme . "://lunr_search");

    if (!is_dir($filesPath)) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage('Lunr files not found. Ensure an index has been run.', $messenger::TYPE_WARNING);
      return [];
    }

    $files = [];
    foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($filesPath)) as $filename) {
      if ($filename->isDir()) {
        continue;
      }
      $files[] = str_replace(DRUPAL_ROOT, '', $filename->getPathname());
    }

    $files[] = '/' . \Drupal::service('extension.list.module')->getPath('lunr') . '/js/search.worker.js';
    $files[] = '/' . \Drupal::service('extension.list.module')->getPath('lunr') . '/js/vendor/lunr/lunr.min.js';

    return $files;
  }

  /**
   * Find Lunr routes.
   *
   * Determine URLs Lunr indexes are exposed on.
   */
  public static function findLunrRoutes() {
    $lunr_storage = \Drupal::service('entity_type.manager')->getStorage('lunr_search');
    $routes = [];

    foreach ($lunr_storage->loadMultiple() as $search) {
      $routes[] = $search->getPath();
    }

    return $routes;
  }

  /**
   * Add/update redirect via API request.
   */
  public static function seedRedirect($redirect) {

    // If the source path has changed, unpublish the old path as the redirect
    // from that path no longer works in Drupal.
    if (!$redirect->isNew()) {
      $originalSource = $redirect->original->getSourcePathWithQuery();
      if ($originalSource && $originalSource != $redirect->getSourcePathWithQuery()) {
        Utility::unpublishUrl($originalSource, 'Unpublished redirect');
      }
    }

    // @todo For "all language" redirects, the non-prefixed redirect is missing.
    $redirects = self::getRedirectLocationsFromRedirect($redirect);
    foreach ($redirects as $r) {
      $event = new QuantRedirectEvent($r['source'], $r['destination'], $r['status_code']);
      \Drupal::service('event_dispatcher')->dispatch($event, QuantRedirectEvent::UPDATE);
    }
  }

  /**
   * Get all redirects from a redirect entity.
   *
   * For multilingual sites using path prefixes for language negotiation, the
   * language's path prefix needs to be handled and, in some cases, there need
   * to be multiple redirects, one for each language.
   *
   * @return array
   *   The list of redirects.
   */
  public static function getRedirectLocationsFromRedirect($redirect) {
    $redirects = [];

    $source = $redirect->getSourcePathWithQuery();
    $destination = $redirect->getRedirectUrl()->toString();
    $statusCode = $redirect->getStatusCode();

    // If site does not use prefixes, return single redirect item.
    if (!Utility::usesLanguagePathPrefixes()) {
      $redirects[] = [
        'source' => $source,
        'destination' => $destination,
        'status_code' => $statusCode,
      ];
      return $redirects;
    }

    // Get language and prefix configuration.
    $langcode = $redirect->language()->getId();
    $defaultLangcode = \Drupal::service('language.default')->get()->getId();
    $pathPrefixes = \Drupal::config('language.negotiation')->get('url.prefixes');
    $defaultPrefix = $pathPrefixes[$defaultLangcode] ?? '';

    // Multilingual redirects can be configured for a specific language or
    // for all languages. If the redirect is configured for all languages,
    // create a redirect for each language.
    $langcodes = [$langcode];
    if ($langcode === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      $langcodes = array_keys(\Drupal::languageManager()->getLanguages());
    }

    // Check if a node or term for this path exists.
    $node = NULL;
    $term = NULL;
    $aliasWithoutPrefix = preg_replace('/^\/(' . $defaultPrefix . ')\//', '/', $destination);
    $path = \Drupal::service('path_alias.manager')->getPathByAlias($aliasWithoutPrefix);
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      $node = Node::load($matches[1]);
    }
    elseif (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches)) {
      $term = Term::load($matches[1]);
    }

    // Create multilingual redirects.
    foreach ($langcodes as $langcode) {
      // Path prefix might not be the same as the langcode. For the default
      // language, the path prefix might not be set.
      $pathPrefix = $pathPrefixes[$langcode] ? '/' . $pathPrefixes[$langcode] : '';
      $updatedSource = $pathPrefix . $source;

      // For nodes and terms, get alias associated with the langcode, if any.
      if ($node || $term) {
        $path = $node ? '/node/' . $node->id() : '/taxonomy/term/' . $term->id();
        $alias = \Drupal::service('path_alias.manager')->getAliasByPath($path, $langcode);
        if ($alias == $path) {
          // No alias exists. Note there is currently a Drupal core bug #1125428
          // that prevents getting the alias in some cases.
          // @todo Work around the core bug.
          $updatedDestination = $destination;
        }
        else {
          // Add the prefix to the correct alias.
          $updatedDestination = $pathPrefix . $alias;
        }
      }
      // @todo Test use case where page is not a node or term.
      else {
        $updatedDestination = preg_replace('/^\/(' . $defaultPrefix . ')\//', $pathPrefix . '/', $destination);
      }
      $redirects[] = [
        'source' => $updatedSource,
        'destination' => $updatedDestination,
        'status_code' => $statusCode,
      ];
    }
    return $redirects;
  }

  /**
   * Unpublish existing redirects via API request.
   */
  public static function unpublishRedirect($redirect) {
    $redirects = self::getRedirectLocationsFromRedirect($redirect);
    foreach ($redirects as $r) {
      // QuantEvent can be used to unpublish any resource. Note, the source must
      // be given here and not the destination.
      Utility::unpublishUrl($r['source'], 'Unpublished redirect');
    }
  }

  /**
   * Seeds taxonomy term.
   */
  public static function seedTaxonomyTerm($entity, $langcode = NULL) {
    $tid = $entity->get('tid')->value;

    $url = Utility::getCanonicalUrl('taxonomy_term', $tid, $langcode);
    $response = self::markupFromRoute($url);

    if (empty($response)) {
      // The markupFromRoute function works differently for unpublished terms
      // versus nodes. If the response is empty, the term is unpublished.
      Utility::unpublishUrl($url, 'Unpublished taxonomy term page');

      return;
    }

    $meta = [];
    [$markup, $content_type] = $response;

    if (!empty($content_type)) {
      $meta['content_type'] = $content_type;
    }

    $metaManager = \Drupal::service('plugin.manager.quant.metadata');
    foreach ($metaManager->getDefinitions() as $pid => $def) {
      $plugin = $metaManager->createInstance($pid);
      if ($plugin->applies($entity)) {
        $meta = array_merge($meta, $plugin->build($entity, $langcode));
      }
    }

    $published = $entity->isPublished();
    if ($published) {
      \Drupal::service('event_dispatcher')->dispatch(new QuantEvent($markup, $url, $meta, NULL, $entity, $langcode), QuantEvent::OUTPUT);
    }
    else {
      Utility::unpublishUrl($url, 'Unpublished taxonomy term page');
    }

    // Handle internal path redirects.
    self::handleInternalPathRedirects($entity, $langcode, $url);
  }

  /**
   * Trigger an internal HTTP request to retrieve node markup.
   *
   * Seeds an individual node update to Quant.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A node interface.
   * @param string $langcode
   *   The node language.
   */
  public static function seedNode(EntityInterface $entity, $langcode = NULL) {

    $nid = $entity->get('nid')->value;
    $rid = $entity->get('vid')->value;

    $url = Utility::getCanonicalUrl('node', $nid, $langcode);
    $defaultLangcode = \Drupal::languageManager()->getDefaultLanguage()->getId();

    // If this is the front/home page, rewrite URL as /.
    $site_config = \Drupal::config('system.site');
    $front = $site_config->get('page.front');

    if ((strpos($front, '/node/') === 0) && $nid == substr($front, 6)) {
      if ($entity->isPublished() && $entity->isDefaultRevision()) {
        // Trigger redirect event from alias to /.
        \Drupal::service('event_dispatcher')->dispatch(new QuantRedirectEvent($url, "/", 301), QuantRedirectEvent::UPDATE);
      }

      $url = "/";

      // Handle default language prefix.
      if ($langcode == $defaultLangcode) {
        // Tack on the prefix if it's set.
        $negotiation = \Drupal::config('language.negotiation')->get('url');
        $url .= $negotiation['prefixes'][$langcode] ?? '';
        if ($url != "/") {
          \Drupal::service('event_dispatcher')->dispatch(new QuantRedirectEvent("/", $url, 301), QuantRedirectEvent::UPDATE);
          \Drupal::logger('quant_seed')->notice("Adding home page redirect: / => @url", ['@url' => $url]);
        }
      }
      // Handle translated front/home page.
      elseif ($prefix = Utility::getPathPrefix($langcode)) {
        $url = $prefix;
        \Drupal::logger('quant_seed')->notice("Adding translated home page: @url", ['@url' => $url]);
      }
    }

    $response = self::markupFromRoute($url, ['quant-revision' => $rid]);
    $meta = [];

    if (empty($response)) {
      return;
    }

    [$markup, $content_type] = $response;

    if (!empty($content_type)) {
      $meta['content_type'] = $content_type;
    }

    $metaManager = \Drupal::service('plugin.manager.quant.metadata');
    foreach ($metaManager->getDefinitions() as $pid => $def) {
      $plugin = $metaManager->createInstance($pid);
      if ($plugin->applies($entity)) {
        $meta = array_merge($meta, $plugin->build($entity, $langcode));
      }
    }

    // Handle status pages. Must happen after response has been checked.
    $statusPages = [
      '/_quant403' => $site_config->get('page.403'),
      '/_quant404' => $site_config->get('page.404'),
    ];

    // If this node is a status page, rewrite URL to use special internal route
    // so they show up properly when getting a 403 or 404 status code.
    foreach ($statusPages as $key => $value) {
      if ((strpos($value, '/node/') === 0) && $entity->get('nid')->value == substr($value, 6)) {
        // Only set for the default language.
        // @todo Handle translated status pages.
        if (empty($langcode) || $langcode == $defaultLangcode) {
          $url = $key;
          \Drupal::logger('quant')->notice("Setting status page: @key => @value",
            [
              '@key' => $key,
              '@value' => $value,
            ]
          );
        }
      }
    }

    // Unpublish if necessary.
    $published = $entity->isPublished();
    if ($published) {
      \Drupal::service('event_dispatcher')->dispatch(new QuantEvent($markup, $url, $meta, $rid, $entity, $langcode), QuantEvent::OUTPUT);
    }
    else {
      Utility::unpublishUrl($url, 'Unpublished content page');
    }

    // Handle internal path redirects.
    self::handleInternalPathRedirects($entity, $langcode, $url);
  }

  /**
   * Unpublish the node path from Quant.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The node entity.
   */
  public static function unpublishNode(EntityInterface $entity) {

    $langcode = $entity->language()->getId();
    $nid = $entity->get('nid')->value;
    $url = Utility::getCanonicalUrl('node', $nid, $langcode);

    $site_config = \Drupal::config('system.site');
    $front = $site_config->get('page.front');
    if ((strpos($front, '/node/') === 0) && $nid == substr($front, 6)) {
      Utility::unpublishUrl('/', 'Unpublished home page');
    }

    // Handle internal path redirects.
    self::handleInternalPathRedirects($entity, $langcode, $url);

    Utility::unpublishUrl($url, 'Unpublished content page');
  }

  /**
   * Unpublish the term path from Quant.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The term entity.
   */
  public static function unpublishTaxonomyTerm(EntityInterface $entity) {

    $langcode = $entity->language()->getId();
    $tid = $entity->get('tid')->value;
    $url = Utility::getCanonicalUrl('taxonomy_term', $tid, $langcode);

    // Handle internal path redirects.
    self::handleInternalPathRedirects($entity, $langcode, $url);

    Utility::unpublishUrl($url, 'Unpublished taxonomy term page');
  }

  /**
   * Unpublish the file from Quant.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The file entity.
   */
  public static function unpublishFile(EntityInterface $entity) {

    $url = $entity->createFileUrl();

    Utility::unpublishUrl($url, 'Unpublished file');

    // If the file is an image, unpublish any image styles.
    $uri = $entity->getFileUri();
    $styles = ImageStyle::loadMultiple();
    foreach ($styles as $style) {
      if ($style->supportsUri($uri)) {
        // Don't check filesystem because files have been deleted by this point.
        $path = parse_url($style->buildUrl($uri))['path'];

        Utility::unpublishUrl($path, 'Unpublished image style');
      }
    }
  }

  /**
   * Unpublish the media from Quant.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The media entity.
   */
  public static function unpublishMedia(EntityInterface $entity) {

    // @todo Handle custom media types or ones from other modules. Could grab
    // all fields through introspection and check if it's a file field.
    $fields = [
      'field_media_audio_file',
      'field_media_document',
      'field_media_image',
      'field_media_video_file',
    ];

    foreach ($fields as $field) {
      if ($entity->hasField($field)) {
        $file = $entity->get($field)->entity;
        if ($file) {
          self::unpublishFile($file);
        }
      }
    }
  }

  /**
   * Unpublish the view paths from Quant.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The view entity.
   */
  public static function unpublishView(EntityInterface $entity) {
    // Go through all displays to find pages.
    $displays = [];
    foreach ($entity->get('display') as $display_id => $display) {
      if (!empty($display['display_options']['path'])) {
        $displays[] = $display_id;
      }
    }

    // Unpublish main path and all pager pages.
    $view = Views::getView($entity->id());
    foreach ($displays as $display_id) {
      $view->setDisplay($display_id);

      // Get the pager settings.
      $pager = $view->display_handler->getOption('pager');
      $type = $pager['type'];
      $items_per_page = $pager['options']['items_per_page'];

      // Don't process if not using pager.
      if ($type == 'some' || $type == 'none') {
        continue;
      }

      // Switch to not using a pager to get all results.
      $view->display_handler->setOption('pager', [
        'type' => 'none',
        'options' => [
          'offset' => 0,
        ],
      ]);

      // Get the number of pages.
      $view->execute();
      $count = count($view->result);
      $total_pages = ceil($count / $items_per_page);

      // Construct the URLs.
      $path = $view->getPath();
      for ($i = 0; $i < $total_pages; $i++) {
        // Handle multilingual paths prefixes and no prefix.
        $prefixes = \Drupal::config('language.negotiation')->get('url.prefixes') ?? [];
        $prefixes = array_unique([''] + $prefixes);
        foreach ($prefixes as $prefix) {
          if ($prefix) {
            $prefix = "/{$prefix}";
          }

          // Handle the base path.
          if ($i === 0) {
            $url = "{$prefix}/{$path}";

            Utility::unpublishUrl($url, 'Unpublished views page');
          }

          // Handle the pager path.
          $pager_url = "{$prefix}/{$path}?page={$i}";

          Utility::unpublishUrl($pager_url, 'Unpublished views page');
        }
      }
    }
  }

  /**
   * Unpublish path alias via API request.
   */
  public static function unpublishPathAlias($pathAlias) {

    $langcode = $pathAlias->get('langcode')->value;
    $alias = Utility::getUrl($pathAlias->get('alias')->value, $langcode);

    // Strip the 'und'.
    if ($langcode == LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      $alias = str_replace("/{$langcode}", '', $alias);
    }

    Utility::unpublishUrl($alias, 'Unpublished path alias');
  }

  /**
   * Handle internal path redirects.
   *
   * Example redirects:
   * - en node 123, no alias: /node/123 to /en/node/123.
   * - es node 123, no alias: /node/123 to /es/node/123.
   * - en node 123, alias: /node/123 and /en/node/123 to en alias.
   * - es node 123, alias: /node/123 and /es/node/123 to es alias.
   * - en node 123, es translation, no alias: /node/123 to /en/node/123.
   * - en node 123, es translation, alias: /node/123 and /en/node/123 to en
   *   alias, /es/node/123 to es alias.
   *
   * @todo Create simpler logic for when multilingual isn't used?
   */
  public static function handleInternalPathRedirects($entity, $langcode, $url) {
    $type = $entity->getEntityTypeId();
    if (!in_array($type, ['node', 'taxonomy_term'])) {
      \Drupal::logger('quant_seed')->error('Quant: handleInternalPathRedirects called with wrong type [@type]', ['@type' => $type]);
      return;
    }

    $id = $entity->id();
    $published = $entity->isPublished();
    $internalPath = ($type == 'node') ? "/node/{$id}" : "/taxonomy/term/{$id}";
    $prefix = Utility::getPathPrefix($langcode);

    // If there is default language content, then the internal path redirect can
    // use the default URL. Otherwise, it should use the current language.
    // Note, the canonical URL is the alias, if it exists, or the internal path.
    $defaultLanguage = \Drupal::languageManager()->getDefaultLanguage();
    $defaultUrl = Url::fromRoute('entity.' . $type . '.canonical', [$type => $id], ['language' => $defaultLanguage])->toString();
    $defaultTranslation = $entity->hasTranslation($defaultLanguage->getId()) ? $entity->getTranslation($defaultLanguage->getId()) : NULL;
    $defaultPublished = $defaultTranslation ? $defaultTranslation->isPublished() : $published;
    $language = \Drupal::languageManager()->getLanguage($langcode);
    $languageUrl = Url::fromRoute('entity.' . $type . '.canonical', [$type => $id], ['language' => $language])->toString();
    if (!$defaultTranslation) {
      $defaultUrl = $languageUrl;
    }

    // Only create redirects if the content has an alias.
    if ($internalPath != $url) {
      \Drupal::service('event_dispatcher')->dispatch(new QuantRedirectEvent($internalPath, $defaultUrl, 301), QuantRedirectEvent::UPDATE);
      if ($prefix) {
        // Handle redirects with path prefix too.
        \Drupal::service('event_dispatcher')->dispatch(new QuantRedirectEvent("/{$prefix}{$internalPath}", $languageUrl, 301), QuantRedirectEvent::UPDATE);
      }
    }

    // Unpublish redirects.
    if (!$defaultPublished) {
      Utility::unpublishUrl($internalPath, 'Unpublished internal path');
    }
    if (!$published && $prefix) {
      // Handle redirects with path prefix too.
      Utility::unpublishUrl("/{$prefix}{$internalPath}", 'Unpublished internal path');
    }
  }

  /**
   * Attempts a HTTP HEAD request to a given route.
   *
   * @param string $route
   *   The route to poll.
   * @param array $headers
   *   Headers to add to the request.
   *
   * @return bool
   *   Whether a 200 response was received or not.
   */
  public static function headRoute($route, array $headers = []) {

    // Build internal request.
    $config = \Drupal::config('quant.settings');
    $local_host = $config->get('local_server') ?: 'http://localhost';
    $hostname = $config->get('host_domain') ?: $_SERVER['SERVER_NAME'];
    $url = $local_host . $route;

    $headers['Host'] = $hostname;

    // Generate a signed token and use it in the request. This only applies when
    // drafts are enabled, as we return neutral access otherwise.
    $disable_drafts = $config->get('disable_content_drafts');
    if (!$disable_drafts) {
      $headers['quant-token'] = \Drupal::service('quant.token_manager')->create($route);
    }

    // Support basic auth if enabled (note: will not work via drush/cli).
    $auth = !empty($_SERVER['PHP_AUTH_USER']) ? [
      $_SERVER['PHP_AUTH_USER'],
      $_SERVER['PHP_AUTH_PW'],
    ] : [];

    try {
      $response = \Drupal::httpClient()->head($url, [
        'http_errors' => FALSE,
        'headers' => $headers,
        'auth' => $auth,
        'allow_redirects' => FALSE,
        'verify' => boolval($config->get('ssl_cert_verify')),
      ]);
    }
    catch (ConnectException $exception) {
      return FALSE;
    }

    if ($response->getStatusCode() == 200) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns markup for a given route.
   *
   * @param string $route
   *   The route to collect markup from.
   * @param array $headers
   *   Headers to add to the request.
   *
   * @return string|bool
   *   The markup from the $route.
   */
  public static function markupFromRoute($route, array $headers = []) {

    // Clean double slashes from routes.
    // The exception is oEmbed routes which passes a full URL as query param.
    if (!preg_match('/\/media\/oembed\?url=/', $route)) {
      $route = str_replace('//', '/', $route);
    }

    // Build internal request.
    $config = \Drupal::config('quant.settings');
    $local_host = $config->get('local_server') ?: 'http://localhost';
    $hostname = $config->get('host_domain') ?: $_SERVER['SERVER_NAME'];
    $url = $local_host . $route;

    $headers['Host'] = $hostname;

    // Generate a signed token and use it in the request. This only applies when
    // drafts are enabled, as we return neutral access otherwise.
    $disable_drafts = $config->get('disable_content_drafts');
    if (!$disable_drafts) {
      $headers['quant-token'] = \Drupal::service('quant.token_manager')->create($route);
    }

    // Support basic auth if enabled (note: will not work via drush/cli).
    $auth = !empty($_SERVER['PHP_AUTH_USER']) ? [
      $_SERVER['PHP_AUTH_USER'],
      $_SERVER['PHP_AUTH_PW'],
    ] : [];

    // @todo ; Note: Passing in the Host header fixes issues with absolute links.
    // It may also cause some redirects to the real host.
    // Best to trap redirects and re-run against the final path.
    try {
      $response = \Drupal::httpClient()->post($url, [
        'http_errors' => FALSE,
        'headers' => $headers,
        'auth' => $auth,
        'allow_redirects' => FALSE,
        'verify' => boolval($config->get('ssl_cert_verify')),
      ]);
    }
    catch (ConnectException $exception) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage("Unable to connect to {$url}", $messenger::TYPE_ERROR);
      \Drupal::logger('quant_seed')->notice($exception->getMessage());
      return FALSE;
    }

    $markup = $content_type = '';

    switch ($response->getStatusCode()) {
      case 301:
      case 302:
        $location_header = $response->getHeader('Location');
        $destination = reset($location_header);
        // Ensure relative for internal redirect.
        $destination = self::rewriteRelative($destination);
        \Drupal::service('event_dispatcher')->dispatch(new QuantRedirectEvent($route, $destination, $response->getStatusCode()), QuantRedirectEvent::UPDATE);
        return FALSE;

      case 200:
        $markup = $response->getBody();
        $content_type = $response->getHeader('content-type');
        break;

      case 404:
        if (strpos($url, '_quant') > -1) {
          $markup = $response->getBody();
          $content_type = $response->getHeader('content-type');
          break;
        }

      default:
        $messenger = \Drupal::messenger();
        $messenger->addMessage("Non-200 response for {$route}: " . $response->getStatusCode(), $messenger::TYPE_WARNING);
        \Drupal::logger('quant_seed')->notice("Non-200 response for {$route}: " . $response->getStatusCode());
        return FALSE;
    }

    return [self::rewriteRelative($markup), $content_type];
  }

  /**
   * Replaces absolute URLs with relative in markup.
   *
   * @param string $markup
   *   The markup to search and rewire relative paths for.
   *
   * @return string
   *   Sanitized markup string.
   */
  public static function rewriteRelative($markup) {
    $config = \Drupal::config('quant.settings');

    // Do not strip host domain unless configured.
    $strip = $config->get('host_domain_strip') ?: FALSE;
    if (!$strip) {
      // Handle iframes even if `host_domain_strip` is disabled, so iframe
      // renders correctly.
      $markup = self::rewriteRelativeIframe($markup);

      return $markup;
    }

    // Strip the host domain from everywhere in the content including header
    // metadata such as canonical links.
    $markup = Utility::stripLocalHost($markup);

    return $markup;
  }

  /**
   * Replaces absolute iframe URLs with relative in markup.
   *
   * @param string $markup
   *   The markup to search and rewire relative iframe paths for.
   *
   * @return string
   *   Sanitized markup string.
   */
  public static function rewriteRelativeIframe($markup) {
    $pattern = '/<iframe src="([^"]+)"/i';
    if (preg_match_all($pattern, $markup, $matches)) {
      foreach ($matches[1] as $url) {
        $updated_url = Utility::stripLocalHost($url);
        $markup = str_replace($url, $updated_url, $markup);
      }
    }

    return $markup;
  }

}
